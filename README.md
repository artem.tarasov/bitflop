# Bitflop

bitflop is a command-line tool that allows to maintain a directory of torrent downloads as a YAML file.

## Config

Add `~/.config/bitflop/config.yml` with the following content:

```
rpc_url: "http://A.B.C.D:6800/transmission/rpc"  # A.B.C.D is the address of your Transmission server
rpc_username: <YOUR USERNAME>
rpc_password: <YOUR PASSWORD>
trackers:
  # add trackers if you prefer to keep your collections as pure URNs instead of full magnet links
  - name: rutracker  
    servers:
    - "http://bt.t-ru.org/ann?magnet"
    - "http://bt2.t-ru.org/ann?magnet"
    - "http://bt3.t-ru.org/ann?magnet"
    - "http://bt4.t-ru.org/ann?magnet"
collections:
  /mnt/torrents/music: "/home/username/music.yml"
  /mnt/torrents/movies: "/home/username/movies.yml"
```

## Collections

Each collection is a simple YAML file, describing arbitrarily nested directory structure, e.g.:

```
---
Death Grips:
    Bottomless Pit: urn:btih:39559E867FAAC85EB97BFA00485B755125C5EAE5
    Exmilitary: urn:btih:BAD72D6C8450B112CC01D1BB01FB0058DCE82CBB
    The Money Store: urn:btih:C1F314D83257E31EE276EDCB1880BCAFEAD279B8
    Year of the Snitch: urn:btih:E0BDC1A74460944F2D4ADA9F28F3A467D8EEFD52
Siouxsie and the Banshees:
    Hyaena: urn:btih:39E90A015627D7E36DAD1E19CD0F9AE946E94208
    Tinderbox: urn:btih:D8F4766F864B195435DA5F9CDAA6A0C28283F5A6
    Kaleidoscope: urn:btih:9B2223E5C94E32420DAEBA07BBEC931D7A64DE21
    Peepshow: urn:btih:A70033AB057EA3298DA77B1681756154128F41FE
    The Rapture: urn:btih:ABDAAAC6687CDC30B3894A8D8AE053B89F3C8C52
    The Scream: urn:btih:443D92373048CFCF3524CE37144D85478F038A62
    Superstition: urn:btih:B7B2FC0068598AADEEB63D2261262D0089B96920
```

If you don't care about your YAML looking nice, you can simply populate it with full magnet links, e.g.:

```
---
Bulgaria:
  Zift: magnet:?xt=urn:btih:B056681418544914E0832AF642761C516E00FF65&tr=http%3A%2F%2Fbt.t-ru.org%2Fann%3Fmagnet&dn=%D0%94%D0%B7%D0%B8%D1%84%D1%82%20%2F%20%D0%93%D1%83%D0%B4%D1%80%D0%BE%D0%BD%20%2F%20Zift%20(%D0%AF%D0%B2%D0%BE%D1%80%20%D0%93%D1%8B%D1%80%D0%B4%D0%B5%D0%B2%20%2F%20Javor%20Gardev)%20%5B2008%2C%20%D0%91%D0%BE%D0%BB%D0%B3%D0%B0%D1%80%D0%B8%D1%8F%2C%20%D0%BD%D1%83%D0%B0%D1%80%2C%20BDRip%20720p%5D%202x%20DVO%20%2B%20Original%20%2B%20Sub%20Rus%2C%20Eng
Hungary:
  The Notebook: magnet:?xt=urn:btih:9A3FA19EC22A5E37618967766C65E807AA56B0ED&tr=http%3A%2F%2Fbt2.t-ru.org%2Fann%3Fmagnet&dn=%D0%A2%D0%BE%D0%BB%D1%81%D1%82%D0%B0%D1%8F%20%D1%82%D0%B5%D1%82%D1%80%D0%B0%D0%B4%D1%8C%20%2F%20A%20nagy%20f%C3%BCzet%20%2F%20A%20nagy%20fuzet%20%2F%20The%20Notebook%20(%D0%AF%D0%BD%D0%BE%D1%88%20%D0%A1%D0%B0%D1%81%20%2F%20J%C3%A1nos%20Sz%C3%A1sz)%20%5B2013%2C%20%D0%92%D0%B5%D0%BD%D0%B3%D1%80%D0%B8%D1%8F%2C%20%D0%93%D0%B5%D1%80%D0%BC%D0%B0%D0%BD%D0%B8%D1%8F%2C%20%D0%90%D0%B2%D1%81%D1%82%D1%80%D0%B8%D1%8F%2C%20%D0%A4%D1%80%D0%B0%D0%BD%D1%86%D0%B8%D1%8F%2C%20%D0%B4%D1%80%D0%B0%D0%BC%D0%B0%2C%20%D0%B2%D0%BE%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9%2C%20WEB-
```

## Usage

Once you created the config and collection YAMLs, simply run the executable. No arguments are required or expected.

If you use Linux with systemd, it's highly recommended to configure path-based activation.

You'll need one unit for the (oneshot) service:
```
[Unit]
Description=bitflop
After=transmission.target

[Service]
Type=oneshot
ExecStart=/usr/local/bin/bitflop
```

And another for watching the paths:
```
[Path]
PathChanged=%h/.config/bitflop/config.yml
PathChanged=%h/.config/bitflop/collections/music.yml

[Install]
WantedBy=default.target
```

Run `systemctl daemon-reload && systemctl enable --now bitflop.path`, and you are all set!