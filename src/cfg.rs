use serde::Deserialize;

use std::collections::HashMap;

#[derive(Clone, Deserialize)]
pub struct Tracker {
    pub name: String,
    pub servers: Vec<String>,
}

#[derive(Clone, Deserialize)]
pub struct Config {
    pub rpc_url: String,
    pub rpc_username: String,
    pub rpc_password: String,

    // key = mount point;
    // value = path to the YAML file with magnet links
    pub collections: HashMap<String, String>,

    pub trackers: Vec<Tracker>,
}
