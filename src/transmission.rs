use std::collections::HashMap;

use reqwest::Url;

use reqwest::{header::HeaderValue, Response};
use serde::{Deserialize, Serialize};

pub const TRANSMISSION_ID_HEADER: &str = "X-Transmission-Session-Id";

#[derive(Debug, Serialize)]
pub struct GetTorrent {
    #[serde(skip_serializing_if = "Option::is_none")]
    ids: Option<Vec<u32>>,
    fields: Vec<String>,
}

#[derive(Clone, Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct AddTorrent {
    pub filename: String,
    pub download_dir: String,
}

#[derive(Debug, Serialize)]
pub struct MoveTorrent {
    ids: Vec<u32>,
    location: String,
    #[serde(rename = "move")]
    move_: bool,
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum Args {
    Add(AddTorrent),
    Get(GetTorrent),
    Move(MoveTorrent),
}

#[derive(Debug, Serialize)]
pub struct Request {
    pub method: String,
    pub arguments: Args,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct File {
    pub bytes_completed: usize,
    pub length: usize,
    pub name: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Torrent {
    pub id: u32,
    pub magnet_link: Option<String>,
    pub files: Option<Vec<File>>,
    pub is_finished: Option<bool>,
    pub download_dir: Option<String>,
}

fn extract_urn(url: &Url) -> String {
    let xt = url.query_pairs().find(|p| p.0 == "xt");
    if let Some(entry) = xt {
        entry.1.to_lowercase()
    } else {
        "".into()
    }
}

impl Torrent {
    fn urn(&self) -> String {
        if let Some(urn) = &self.magnet_link {
            if let Ok(url) = Url::parse(urn) {
                return extract_urn(&url);
            }
        }
        "".into()
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct TransmissionResult {
    pub torrents: Option<Vec<Torrent>>,
    pub torrent_added: Option<Torrent>,
    pub torrent_duplicate: Option<Torrent>,
}

#[derive(Debug, Deserialize)]
pub struct TransmissionResponse {
    pub arguments: Option<TransmissionResult>,
    pub result: String,
    pub tag: Option<String>,
}

pub struct Session {
    rpc_url: String,
    username: String,
    password: String,
    session_id: Option<HeaderValue>,
    client: reqwest::Client,
}

impl Session {
    pub fn new(rpc_url: &str, username: &str, password: &str) -> Self {
        Self {
            rpc_url: rpc_url.into(),
            username: username.into(),
            password: password.into(),
            session_id: None,
            client: reqwest::Client::new(),
        }
    }

    fn make_req_builder(&self, req: &Request) -> reqwest::RequestBuilder {
        let req_builder = self
            .client
            .post(&self.rpc_url)
            .basic_auth(&self.username, Some(&self.password))
            .json(req);

        if let Some(session_id) = &self.session_id {
            req_builder.header(TRANSMISSION_ID_HEADER, session_id)
        } else {
            req_builder
        }
    }

    pub async fn post(&mut self, req: &Request) -> reqwest::Result<TransmissionResponse> {
        let mut res: Response = self.make_req_builder(req).send().await?;

        if res.status() == 409 {
            if let Some(session_id) = res.headers().get(TRANSMISSION_ID_HEADER) {
                self.session_id = Some(session_id.clone());
                res = self.make_req_builder(req).send().await?;
            }
        }

        res.json::<TransmissionResponse>().await
    }

    pub async fn get_known_urns(&mut self) -> Option<HashMap<String, Torrent>> {
        let req = Request {
            method: "torrent-get".into(),
            arguments: Args::Get(GetTorrent {
                ids: None,
                fields: vec!["id".into(), "magnetLink".into(), "downloadDir".into()],
            }),
        };

        let mut urns = HashMap::<String, Torrent>::new();

        // FIXME: add error handling
        if let Ok(res) = self.post(&req).await {
            for torrent in res.arguments?.torrents?.iter() {
                urns.insert(torrent.urn(), torrent.clone());
            }
        }

        Some(urns)
    }

    pub async fn execute(&mut self, requests: &[Request]) {
        let mut added = 0;
        let mut moved = 0;

        for req in requests.iter() {
            match req.arguments {
                Args::Add(_) => added += 1,
                Args::Move(_) => moved += 1,
                _ => break,
            }
        }

        if added > 0 {
            log::info!("To add: {} torrents", added);
        }
        if moved > 0 {
            log::info!("To move: {} torrents", moved);
        }

        for req in requests.iter() {
            match self.post(req).await {
                Ok(_) => match req.arguments {
                    Args::Add(_) => added -= 1,
                    Args::Move(_) => moved -= 1,
                    _ => break,
                },
                Err(err) => {
                    log::error!("Failure: {:?}", err);
                }
            }
        }

        if added > 0 {
            log::warn!("Failed to add: {} torrents", added);
        }
        if moved > 0 {
            log::warn!("Failed to move: {} torrents", moved);
        }
    }
}

pub struct Update<'a> {
    urns: &'a mut HashMap<String, Torrent>,
    pub requests: Vec<Request>,
}

impl<'a> Update<'a> {
    pub fn new(known_urns: &'a mut HashMap<String, Torrent>) -> Self {
        Self { urns: known_urns, requests: Vec::new() }
    }

    pub fn add(&mut self, url: Url, download_dir: String) {
        let urn = extract_urn(&url);
        if urn.is_empty() {
            log::debug!(
                "Skipping URL {:?} because it has no URN (download dir: {})",
                &url,
                &download_dir
            );
            return;
        }

        if let Some(torrent) = self.urns.get_mut(&urn) {
            let current_dir = torrent.download_dir.as_ref().unwrap();

            if current_dir.eq(&download_dir) {
                log::debug!(
                    "Skipping URN {:?} because it's already added (download dir: {})",
                    &urn,
                    &download_dir
                );
            } else if torrent.id == 0 {
                log::debug!("Updating URN {:?} from {} to {})", &urn, &current_dir, &download_dir);
                torrent.download_dir = Some(download_dir);
            } else {
                log::info!("Moving URN {:?} from {} to {})", &urn, &current_dir, &download_dir);

                self.requests.push(Request {
                    method: "torrent-set-location".into(),
                    arguments: Args::Move(MoveTorrent {
                        ids: vec![torrent.id],
                        location: download_dir.clone(),
                        move_: true,
                    }),
                });

                torrent.download_dir = Some(download_dir);
            }
            return;
        }

        self.urns.insert(
            urn.clone(),
            Torrent {
                id: 0,
                magnet_link: Some(urn),
                files: None,
                is_finished: None,
                download_dir: Some(download_dir.clone()),
            },
        );

        self.requests.push(Request {
            method: "torrent-add".into(),
            arguments: Args::Add(AddTorrent { filename: url.into(), download_dir }),
        });
    }
}
