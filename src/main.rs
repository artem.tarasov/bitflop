use std::collections::HashMap;
use std::path::PathBuf;

use env_logger::Env;
use yaml_rust::{Yaml, YamlLoader};

use anyhow::Result;
use reqwest::Url;

extern crate bitflop;
use bitflop::cfg;
use bitflop::transmission;

struct UpdateGenerator<'a> {
    config: &'a cfg::Config,
}

impl<'a> UpdateGenerator<'a> {
    pub fn new(config: &'a cfg::Config) -> Self {
        Self { config }
    }

    pub fn magnet_link(&self, xt: &str) -> reqwest::Url {
        let link = format!("magnet:?xt={}", xt);
        let mut url = Url::parse(&link).unwrap();
        for tracker in self.config.trackers.iter() {
            for server in tracker.servers.iter() {
                url.query_pairs_mut().append_pair("tr", server);
            }
        }

        url
    }

    pub fn sync(&self, dir: PathBuf, yaml: &Yaml, update: &mut transmission::Update) {
        self.torrent_add_requests(update, dir, yaml);
    }

    fn torrent_add_requests(&self, update: &mut transmission::Update, dir: PathBuf, yaml: &Yaml) {
        match yaml {
            Yaml::Hash(dict) => {
                for (key, value) in dict.iter() {
                    if let Some(subdir) = key.as_str() {
                        self.torrent_add_requests(update, dir.join(subdir), value);
                    }
                }
            }

            Yaml::String(s) => {
                let url = if s.starts_with("magnet:") {
                    Url::parse(s).ok()
                } else if s.starts_with("urn:") {
                    Some(self.magnet_link(s))
                } else {
                    None
                };

                if let Some(link) = url {
                    update.add(link, dir.to_string_lossy().into());
                }
            }

            _ => {}
        }
    }
}

struct Bitflop {
    config: cfg::Config,
    session: transmission::Session,
    torrents: HashMap<String, transmission::Torrent>,
}

impl Bitflop {
    pub fn new(config: cfg::Config) -> Bitflop {
        let session =
            transmission::Session::new(&config.rpc_url, &config.rpc_username, &config.rpc_password);

        Bitflop { config, session, torrents: HashMap::new() }
    }

    pub async fn add_torrents(&mut self, dir: PathBuf, yaml: &Yaml) {
        if let Some(torrents) = self.session.get_known_urns().await {
            self.torrents = torrents;
            let requests = self.generate_diff(dir, yaml);
            self.session.execute(&requests).await;
        }
    }

    fn generate_diff(&mut self, dir: PathBuf, yaml: &Yaml) -> Vec<transmission::Request> {
        let mut update = transmission::Update::new(&mut self.torrents);
        let gen = UpdateGenerator::new(&self.config);
        gen.sync(dir, yaml, &mut update);
        update.requests
    }

    pub fn load_collections(&self) -> Result<HashMap<String, Yaml>> {
        let mut collections = std::collections::HashMap::new();

        for (dst, src) in self.config.collections.iter() {
            let collection_yaml = std::fs::read_to_string(src)?;
            let collection = YamlLoader::load_from_str(&collection_yaml)?;
            collections.insert(dst.clone(), collection[0].clone());
        }

        Ok(collections)
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let home_dir = std::env::var("HOME")?;
    let config_path = format!("{}/.config/bitflop/config.yml", home_dir);
    let config_yaml = std::fs::read_to_string(config_path)?;

    let mut config: cfg::Config = serde_yaml::from_str(&config_yaml)?;

    let username = std::env::var("RPC_USERNAME");
    let password = std::env::var("RPC_PASSWORD");
    if username.is_ok() && password.is_ok() {
        config.rpc_username = username.unwrap();
        config.rpc_password = password.unwrap();
    }

    let mut bitflop = Bitflop::new(config);

    let collections = bitflop.load_collections()?;
    for (dst, collection) in collections.iter() {
        log::info!("Updating collection at {} ...", &dst);
        bitflop.add_torrents(PathBuf::from(&dst), collection).await;
    }

    Ok(())
}
